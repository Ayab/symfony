<?php

namespace App\Controller;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/user", name="user_")
 */
class UserController extends AbstractController {

    /**
     * @Route("/", name="index", methods={"GET"})
     *
     * @return Response
     */
    public function index(): Response {
        //return new Response('hello world');
        $em = $this->getDoctrine()->getManager();

        $users = $em->getRepository(User::class)->findAll();

        return $this->render('User/index.html.twig', [
            'users' => $users
        ]);
    }

  /*  /**
     * @Route("/show/{id}", name="show", methods={"GET"})
     *
     * @return Response

    public function show($id): Response {

        $em = $this->getDoctrine()->getManager();

        $user = $em->getRepository(User::class)->find($id);
        return $this->render('User/show.html.twig',[
            'user' => $user
        ]);
    }*/

    /**
     * @Route("/show/{user}", name="show", methods={"GET"})
     *
     * @return Response
     */
    public function show(User $user): Response {

        return $this->render('User/show.html.twig',[
            'user' => $user
        ]);
    }
}