<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController {

	/**
	 * @Route("/", name="default_index", methods={"GET"})
	 *
	 * @return Response
	 */
	public function index(): Response {
		//return new Response('hello world');
		return $this->render('Default/index.html.twig');
	}

	/**
	 * @Route("/default/{name}", name="default_name", methods={"GET"}, requirements={"name":"\w+"})
	 *
	 * @var string $name
	 * @return Response
	 */
	public function name(string $name): Response {
		//return new Response('hello '.$name);
		return $this->render('Default/name.html.twig', [
			'name' => $name
		]);
	}

}